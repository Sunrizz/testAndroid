package ru.ithaus.ticket.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelDB.DBCards;
import ru.pa_resultant.transportticket.R;

import java.util.List;

/**
 * Адаптер для отображения списка карт
 */

public class AdapterCard extends BaseAdapter {

    Context context;
    LayoutInflater lInflater;
    List<DBCards> objects;

    public AdapterCard(Context context, List<DBCards> dbCards) {
        this.context = context;
        objects = dbCards;
        lInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;

        final DBCards p = ((DBCards) getItem(position));

        if (view == null) {
            view = lInflater.inflate(R.layout.item_card, parent, false);
        }

        TextView textViewCardNum = (TextView) view.findViewById(R.id.textViewCardNum);
        TextView textViewCardDateBlock = (TextView) view.findViewById(R.id.textViewCardDateBlock);
        textViewCardDateBlock.setText("в разработке");
        TextView textViewCardAmmountWay = (TextView) view.findViewById(R.id.textViewCardAmmountWay);

        TextView textViewCardEndDate = (TextView) view.findViewById(R.id.textViewCardEndDate);
        //TextView textViewCardAboutBlock = (TextView) view.findViewById(R.id.textViewCardAboutBlock);

        textViewCardNum.setText(p.getKod());
        textViewCardAmmountWay.setText(""+(p.getMaxTripQty()-p.getTripQty()));
        textViewCardEndDate.setText(p.getDateEnd());

        Resources res = GlobalValue.mainActivity.getResources();
        if(position%2==0){
            ((LinearLayout)view.findViewById(R.id.linearLayout)).setBackgroundColor(res.getColor(R.color.colorShadow));
        }else{
            ((LinearLayout)view.findViewById(R.id.linearLayout)).setBackgroundColor(res.getColor(R.color.colorBackListView));
        }

        return view;
    }

}
