package ru.ithaus.ticket.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelDB.DBCache;
import ru.pa_resultant.transportticket.R;

import java.util.List;

/**
 * Адаптер для отображения списка карт
 */

public class AdapterCache extends BaseAdapter {

    Context context;
    LayoutInflater lInflater;
    List<DBCache> objects;

    public AdapterCache(Context context, List<DBCache> dbCaches) {
        this.context = context;
        objects = dbCaches;
        lInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;

        final DBCache p = ((DBCache) getItem(position));

        if (view == null) {
            view = lInflater.inflate(R.layout.item_cache, parent, false);
        }

        TextView textViewSynk        = (TextView) view.findViewById(R.id.textViewSynk);
        TextView textViewNameCommand = (TextView) view.findViewById(R.id.textViewNameCommand);
        TextView textViewTrackId     = (TextView) view.findViewById(R.id.textViewTrackId);
        TextView textViewMileage     = (TextView) view.findViewById(R.id.textViewMileage);
        TextView textViewLatLon      = (TextView) view.findViewById(R.id.textViewLatLon);
        TextView textViewDate        = (TextView) view.findViewById(R.id.textViewDate);
        TextView textViewPQty        = (TextView) view.findViewById(R.id.textViewPQty);
        TextView textViewLogin       = (TextView) view.findViewById(R.id.textViewLogin);
        TextView textViewPassword    = (TextView) view.findViewById(R.id.textViewPassword);
        TextView textViewUID         = (TextView) view.findViewById(R.id.textViewUID);
        TextView textViewKod         = (TextView) view.findViewById(R.id.textViewKod);

        textViewSynk.setText(""+p.getSync());
        textViewNameCommand.setText(""+p.getNameCommand());
        textViewTrackId.setText(""+p.getTrackId());
        textViewMileage.setText(""+p.getMileage());
        textViewLatLon.setText(""+p.getLat()+";"+p.getLon());
        textViewDate.setText(""+p.getDate());
        textViewPQty.setText(""+p.getPassengerQty());
        textViewLogin.setText(""+p.getLogin());
        textViewPassword.setText(""+p.getPassword());
        textViewUID.setText(""+p.getUserId());
        textViewKod.setText(""+p.getKod());

        Resources res = GlobalValue.mainActivity.getResources();
        if(position%2==0){
            ((LinearLayout)view.findViewById(R.id.linearLayout)).setBackgroundColor(res.getColor(R.color.colorShadow));
        }else{
            ((LinearLayout)view.findViewById(R.id.linearLayout)).setBackgroundColor(res.getColor(R.color.colorBackListView));
        }

        return view;
    }

}
