package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.TrackData;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/** Данные трека
 * Created by us
 */

public interface ApiTrackDataPost {
    @FormUrlEncoded
    @POST("api/track/data")
    Call<TrackData> getData(@FieldMap Map<String, String> parameters, @Header("Authorization") String token);
}

