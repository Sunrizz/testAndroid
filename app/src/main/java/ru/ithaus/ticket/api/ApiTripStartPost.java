package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.TripStart;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/** Начало записи трека пассажира
 * Created by us
 */

public interface ApiTripStartPost {
    @FormUrlEncoded
    @POST("api/trip/start")
    Call<TripStart> getData(@FieldMap Map<String, String> parameters, @Header("Authorization") String token);
}

