package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.Cards;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.QueryMap;

/** Получение списка билетов
 * Created by us
 */

public interface ApiCardsDataLastDateGet {
    @GET("api/cards/data")
    Call<Cards> getData(@QueryMap Map<String, String> parameters, @Header("Authorization") String token);
}

