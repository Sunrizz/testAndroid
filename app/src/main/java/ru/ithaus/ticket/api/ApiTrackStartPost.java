package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.TrackStart;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/** Начало записи трека водителя
 * Created by us
 */

public interface ApiTrackStartPost {
    @FormUrlEncoded
    @POST("api/track/start")
    Call<TrackStart> getData(@FieldMap Map<String, String> parameters, @Header("Authorization") String token);
}

