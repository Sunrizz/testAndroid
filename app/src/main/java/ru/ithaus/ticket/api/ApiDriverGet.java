package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.Driver;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/** Получение списка водителей
 * Created by us
 */

public interface ApiDriverGet {
    @GET("api/driver")
    Call<Driver> getData(@Header("Authorization") String token);
}

