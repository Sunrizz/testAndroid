package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.Cards;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/** Получение списка билетов
 * Created by us
 */

public interface ApiCardsDataGet {
    @GET("api/cards/data")
    Call<Cards> getData(@Header("Authorization") String token);
}

