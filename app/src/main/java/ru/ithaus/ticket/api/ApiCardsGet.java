package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.Cards;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

/** Получение билета по номеру
 * Created by us
 */

public interface ApiCardsGet {
    @GET("api/cards/{id}")
    Call<Cards> getData(@Path("id") String id, @Header("Authorization") String token);
}

