package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.Auth;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/** Авторизация водителя
 * Created by us
 */

public interface ApiAuthPost {
    @FormUrlEncoded
    @POST("api/auth")
    Call<Auth> getData(@FieldMap Map<String, String> parameters);
}

