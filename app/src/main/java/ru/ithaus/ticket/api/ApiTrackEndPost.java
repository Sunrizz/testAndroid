package ru.ithaus.ticket.api;

import ru.ithaus.ticket.modelApi.TrackEnd;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/** Окончание записи трека водителя
 * Created by us
 */

public interface ApiTrackEndPost {
    @FormUrlEncoded
    @POST("api/track/end")
    Call<TrackEnd> getData(@FieldMap Map<String, String> parameters, @Header("Authorization") String token);
}

