package ru.ithaus.ticket.ui;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import ru.ithaus.ticket.app.GlobalValue;
import ru.pa_resultant.transportticket.R;

public class MainActivityUpdate extends AppCompatActivity {

    TextView textViewInfo;
    String info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_update);
        init();
    }

    private void init() {
        GlobalValue.mainActivityUpdate = this;
        textViewInfo = (TextView)findViewById(R.id.textViewInfo);
        if(GlobalValue.processUpdate == false){
            //Не открываем это окно если обновление пришло ооочень быстро
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    public void outInfo(String infoText){
        info = infoText;
        Message msg = new Message();
        msg.obj = infoText;
        handler.sendMessage(msg);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            textViewInfo.setText(msg.obj.toString());
        }
    };

}
