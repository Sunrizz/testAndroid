package ru.ithaus.ticket.ui;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.ithaus.ticket.app.GlobalValue;
import ru.pa_resultant.transportticket.R;

public class FragmentGoodWay extends Fragment {
    public View rootView = null;
    TextView textViewInfoCardNum, textViewInfoWayAmmount, textViewInfoDateCard;
    int ammountWay = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_good_way, container, false);
            //Fabric.with(rootView.getContext(), new Crashlytics());
            init();
        }

        return rootView;
    }

    public void init() {

        ammountWay = GlobalValue.ammountWay-GlobalValue.ammount;  //Остаток поездок на карте

        textViewInfoCardNum = (TextView) rootView.findViewById(R.id.textViewInfoCardNum);
        textViewInfoCardNum.setText(GlobalValue.cardNum);

        textViewInfoDateCard = (TextView) rootView.findViewById(R.id.textViewInfoDateCard);
        textViewInfoDateCard.setText(GlobalValue.cardDateEnd);

        textViewInfoWayAmmount = (TextView) rootView.findViewById(R.id.textViewInfoWayAmmount);
        textViewInfoWayAmmount.setText(GlobalValue.getTextFromAmmountWay(ammountWay));

        if(GlobalValue.cardType==1){
            textViewInfoWayAmmount.setText(" до "+GlobalValue.cardDateEnd);
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                GlobalValue.mainActivity.onBackPressed();
            }
        }, 5000);

    }

}
