package ru.ithaus.ticket.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.other.MyGPS;
import ru.ithaus.ticket.other.SyncDataDB;
import ru.pa_resultant.transportticket.R;

import ru.ithaus.ticket.animate.AnimateButtonUpDown;

import android.widget.Toast;

public class FragmentSelectPass extends Fragment {
    public View rootView = null;
    TextView textViewAmmount, textViewInfoCardNum, textViewInfoWayAmmount;
    ImageView imageViewLeft, imageViewRight;
    int ammount = 1;
    int ammountWay = 0;
    TextView textViewStartButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_select_pass, container, false);
            //Fabric.with(rootView.getContext(), new Crashlytics());
            init();
        }

        return rootView;
    }

    public void init() {

        ammountWay = GlobalValue.ammountWay;

        textViewInfoCardNum = (TextView) rootView.findViewById(R.id.textViewInfoCardNum);
        textViewInfoCardNum.setText(GlobalValue.cardNum);

        textViewInfoWayAmmount = (TextView) rootView.findViewById(R.id.textViewInfoWayAmmount);
        textViewInfoWayAmmount.setText(GlobalValue.getTextFromAmmountWay(ammountWay));
        if(GlobalValue.cardType==1){
            textViewInfoWayAmmount.setText(" до "+GlobalValue.cardDateEnd);
        }


        textViewAmmount = (TextView) rootView.findViewById(R.id.textViewAmmount);
        ammount = 1;
        textViewAmmount.setText("" + ammount);

        imageViewLeft = (ImageView) rootView.findViewById(R.id.imageViewLeft);
        imageViewLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                if (ammount > 1) {
                    ammount--;
                    textViewAmmount.setText("" + ammount);
                } else {
                    textViewAmmount.startAnimation(AnimationUtils.loadAnimation(rootView.getContext(), R.anim.shake));
                }
            }
        });

        imageViewRight = (ImageView) rootView.findViewById(R.id.imageViewRight);
        imageViewRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ammountWay > ammount) {
                    new AnimateButtonUpDown(view);
                    ammount++;
                    textViewAmmount.setText("" + ammount);
                } else {
                    textViewAmmount.startAnimation(AnimationUtils.loadAnimation(rootView.getContext(), R.anim.shake));
                }
            }
        });

        textViewStartButton = (TextView) rootView.findViewById(R.id.textViewStartButton);
        textViewStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                openNexActivity();
            }
        });

    }

    /**
     * Открывает запрос на начало поездки
     */
    private void openNexActivity() {
        Intent intent = new Intent(rootView.getContext(), MainActivityAlertDialog.class);
        intent.putExtra("ammount", ammount);
        startActivityForResult(intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }

        Toast.makeText(rootView.getContext(), "Обработка оплаты",
                Toast.LENGTH_LONG).show();

        GlobalValue.ammount = ammount;

        SyncDataDB.updateCardInBase(GlobalValue.cardNum, GlobalValue.ammount);  //Запишем в БД новое количество использованных поездок
        //MyCache.addTripStart(GlobalValue.cardNum, GlobalValue.getCurrentDateFormatUser(), GlobalValue.getRoundGPS(GlobalValue.lat), GlobalValue.getRoundGPS(GlobalValue.lon), ""+GlobalValue.ammount);

        MyGPS.tripStartOnServer(
                GlobalValue.cardNum,
                GlobalValue.getCurrentDateFormatUser(),
                GlobalValue.getRoundGPS(GlobalValue.lat),
                GlobalValue.getRoundGPS(GlobalValue.lon),
                "" + GlobalValue.ammount,
                GlobalValue.userId,
                true,
                null);

        GlobalValue.mainActivity.onOpenFragmenGoodWay();
    }
}
