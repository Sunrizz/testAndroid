package ru.ithaus.ticket.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import ru.pa_resultant.transportticket.R;

import ru.ithaus.ticket.app.GlobalValue;

public class MainActivityAlertDialog extends AppCompatActivity {

    int ammount=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main_alert_dialog);

        ammount = getIntent().getIntExtra("ammount",0);
        ((TextView)findViewById(R.id.textViewInfo)).setText("Будет оплачено: "+ GlobalValue.getTextFromAmmountWay(ammount));

        ((TextView)findViewById(R.id.textViewNo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((TextView)findViewById(R.id.textViewCheckOtherCard)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("name", "yes");
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }
}
