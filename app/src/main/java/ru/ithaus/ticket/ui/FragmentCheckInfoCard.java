package ru.ithaus.ticket.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.ithaus.ticket.animate.AnimateButtonUpDown;
import ru.ithaus.ticket.app.GlobalValue;
import ru.pa_resultant.transportticket.R;

public class FragmentCheckInfoCard extends Fragment {
    public View rootView = null;
    TextView textViewInfoCardNum, textViewInfoWayAmmount, textViewInfoDateCard, textViewCheckOtherCard,  textViewInfoStatus, textViewToWay;
    ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_check_info_card, container, false);
            //Fabric.with(rootView.getContext(), new Crashlytics());
            init();
        }

        return rootView;
    }

    public void init() {

        textViewInfoCardNum = (TextView) rootView.findViewById(R.id.textViewInfoCardNum);
        textViewInfoCardNum.setText(GlobalValue.cardNum);

        textViewInfoStatus = (TextView) rootView.findViewById(R.id.textViewInfoStatus);
        textViewInfoStatus.setText(GlobalValue.cardStatus);

        textViewInfoDateCard = (TextView) rootView.findViewById(R.id.textViewInfoDateCard);
        textViewInfoDateCard.setText(GlobalValue.cardDateEnd);

        textViewInfoWayAmmount = (TextView) rootView.findViewById(R.id.textViewInfoWayAmmount);
        textViewInfoWayAmmount.setText(GlobalValue.getTextFromAmmountWay(GlobalValue.ammountWay));

        textViewCheckOtherCard = (TextView)rootView.findViewById(R.id.textViewCheckOtherCard);
        textViewCheckOtherCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalValue.mainActivity.onBackPressed();
            }
        });

        textViewToWay = (TextView)rootView.findViewById(R.id.textViewToWay);
        textViewToWay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                GlobalValue.mainActivity.openFragmentSelectPass(false);
            }
        });

    }

}
