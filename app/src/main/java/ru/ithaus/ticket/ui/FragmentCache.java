package ru.ithaus.ticket.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelDB.DBCache;
import ru.pa_resultant.transportticket.R;

import ru.ithaus.ticket.adapter.AdapterCache;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Для отладки вызывается командой с QR test cache
 */

public class FragmentCache extends Fragment {
    public View rootView=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView==null) {
            rootView = inflater.inflate(R.layout.fragment_cache , container, false);
            init();
        }

        return rootView;
    }

    public void init() {

        Realm realm = Realm.getDefaultInstance();
        final RealmResults<DBCache> result = realm.where(DBCache.class)
                .findAll();

        Toast.makeText(GlobalValue.mainActivity, "Найдено записей: "+result.size(), Toast.LENGTH_SHORT).show();

        AdapterCache adapterCache = new AdapterCache(rootView.getContext(), result);
        ListView listView = (ListView)rootView.findViewById(R.id.listView);
        listView.setAdapter(adapterCache);

    }

}
