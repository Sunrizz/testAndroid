package ru.ithaus.ticket.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import ru.ithaus.ticket.adapter.AdapterCard;
import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelDB.DBCards;
import ru.pa_resultant.transportticket.R;

import io.realm.Realm;
import io.realm.RealmResults;

public class FragmentBlockCard extends Fragment {
    public View rootView=null;
    public boolean openAll=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView==null) {
            rootView = inflater.inflate(R.layout.fragment_block_card , container, false);
            //Fabric.with(rootView.getContext(), new Crashlytics());
            init();
        }

        return rootView;
    }

    public void init() {

        Realm realm = Realm.getDefaultInstance();
        RealmResults<DBCards> result;
        if(openAll==false) {
            result = realm.where(DBCards.class)
                    .equalTo("isBlocked", 1)
                    .findAll();
        }else {
            result = realm.where(DBCards.class)
                    //.equalTo("isBlocked", 1)
                    .findAll();
        }

        Toast.makeText(GlobalValue.mainActivity, "Найдено карт: "+result.size(), Toast.LENGTH_SHORT).show();

        AdapterCard adapterBlogs = new AdapterCard(rootView.getContext(), result);
        ListView listView = (ListView)rootView.findViewById(R.id.listView);
        listView.setAdapter(adapterBlogs);

    }

}
