package ru.ithaus.ticket.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.gv.BarcodeGraphic;
import ru.pa_resultant.transportticket.R;

import ru.ithaus.ticket.animate.AnimateButtonUpDown;
import ru.ithaus.ticket.gv.BarcodeGraphicTracker;
import ru.ithaus.ticket.gv.CameraSource;
import ru.ithaus.ticket.gv.CameraSourcePreview;
import ru.ithaus.ticket.gv.GraphicOverlay;

import java.io.IOException;

public class FragmentScanCard extends Fragment {
    public View rootView=null;

    private static final String TAG = "Barcode-reader";

    // intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    // constants used to pass extra data in the intent
    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash  = "UseFlash";
    public static final String BarcodeObject = "Barcode";

    public CameraSource mCameraSource;
    public CameraSourcePreview mPreview;
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;

    // helper objects for detecting taps and pinches.
    private ScaleGestureDetector scaleGestureDetector;

    TextView editTexNumCard;
    ImageView imageViewStartWithoutScan;

    TextView  textViewInfo1;
    ImageView imageViewInfo1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(rootView==null) {
            rootView = inflater.inflate(R.layout.fragment_scan_card  , container, false);
            //Fabric.with(rootView.getContext(), new Crashlytics());
            init();
        }

        return rootView;
    }

    public void init() {
        mPreview = (CameraSourcePreview) rootView.findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) rootView.findViewById(R.id.graphicOverlay);
        editTexNumCard = (TextView) rootView.findViewById(R.id.editTexNumCard);

        imageViewStartWithoutScan = (ImageView)rootView.findViewById(R.id.imageViewStartWithoutScan);
        imageViewStartWithoutScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                GlobalValue.mainActivity.onDataFromScan(editTexNumCard.getText().toString());
            }
        });

        // read parameters from the intent used to launch the activity.
        //boolean autoFocus = getIntent().getBooleanExtra(AutoFocus, true);
        //boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);

        boolean autoFocus = true;
        boolean useFlash = false;


        createCameraSource(autoFocus, useFlash);
        startCameraSource();

        textViewInfo1  = (TextView)  rootView.findViewById(R.id.textViewInfo1);
        imageViewInfo1 = (ImageView) rootView.findViewById(R.id.imageViewInfo1);
        showHideInfo("", View.INVISIBLE);
        //gestureDetector = new GestureDetector(this, new CaptureGestureListener());
        //scaleGestureDetector = new ScaleGestureDetector(this, new FragmentScanCard().ScaleListener());

        initNumPad();

    }

    public void initNumPad(){

        ((Button)rootView.findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"1");
            }
        });

        ((Button)rootView.findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"2");
            }
        });

        ((Button)rootView.findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"3");
            }
        });

        ((Button)rootView.findViewById(R.id.button4)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"4");
            }
        });

        ((Button)rootView.findViewById(R.id.button5)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"5");
            }
        });

        ((Button)rootView.findViewById(R.id.button6)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"6");
            }
        });

        ((Button)rootView.findViewById(R.id.button7)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"7");
            }
        });

        ((Button)rootView.findViewById(R.id.button8)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"8");
            }
        });

        ((Button)rootView.findViewById(R.id.button9)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"9");
            }
        });

        ((Button)rootView.findViewById(R.id.button0)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                editTexNumCard.setText(editTexNumCard.getText().toString()+"0");
            }
        });

        ((ImageView)rootView.findViewById(R.id.imageViewBackspace)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AnimateButtonUpDown(view);
                if(editTexNumCard.getText().toString().length()!=0) {
                    editTexNumCard.setText(editTexNumCard.getText().toString().substring(0, editTexNumCard.getText().toString().length() - 1));
                }
            }
        });

    }

    public void showHideInfo(String strInfo, int visible){
        textViewInfo1.setText(strInfo);
        textViewInfo1.setVisibility(visible);
        imageViewInfo1.setVisibility(visible);
    }

    @Override
    public void onResume() {
        super.onResume();

        startCameraSource();

        /*boolean autoFocus = true;
        boolean useFlash = false;
        createCameraSource(autoFocus, useFlash);
        startCameraSource();
        showHideInfo("", View.INVISIBLE);*/
    }

    /**
     * Stops the camera.
     */
    @Override
    public void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }


    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }


    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    public void startCameraSource() throws SecurityException {
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                GlobalValue.mainActivity.getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(GlobalValue.mainActivity, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    /**
     * onTap returns the tapped barcode result to the calling Activity.
     *
     * @param rawX - the raw position of the tap
     * @param rawY - the raw position of the tap.
     * @return true if the activity is ending.
     */
    private boolean onTap(float rawX, float rawY) {
        // Find tap point in preview frame coordinates.
        int[] location = new int[2];
        mGraphicOverlay.getLocationOnScreen(location);
        float x = (rawX - location[0]) / mGraphicOverlay.getWidthScaleFactor();
        float y = (rawY - location[1]) / mGraphicOverlay.getHeightScaleFactor();

        // Find the barcode whose center is closest to the tapped point.
        Barcode best = null;
        float bestDistance = Float.MAX_VALUE;
        for (BarcodeGraphic graphic : mGraphicOverlay.getGraphics()) {
            Barcode barcode = graphic.getBarcode();
            if (barcode.getBoundingBox().contains((int) x, (int) y)) {
                // Exact hit, no need to keep looking.
                best = barcode;
                break;
            }
            float dx = x - barcode.getBoundingBox().centerX();
            float dy = y - barcode.getBoundingBox().centerY();
            float distance = (dx * dx) + (dy * dy);  // actually squared distance
            if (distance < bestDistance) {
                best = barcode;
                bestDistance = distance;
            }
        }


        if (best != null) {
            Intent data = new Intent();
            data.putExtra(BarcodeObject, best);
            //setResult(CommonStatusCodes.SUCCESS, data);
            //finish();
            return true;
        }
        return false;
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {

        /**
         * Responds to scaling events for a gesture in progress.
         * Reported by pointer motion.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should consider this event
         * as handled. If an event was not handled, the detector
         * will continue to accumulate movement until an event is
         * handled. This can be useful if an application, for example,
         * only wants to update scaling factors if the change is
         * greater than 0.01.
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            Log.i("tester", "onScale");
            return false;
        }

        /**
         * Responds to the beginning of a scaling gesture. Reported by
         * new pointers going down.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should continue recognizing
         * this gesture. For example, if a gesture is beginning
         * with a focal point outside of a region where it makes
         * sense, onScaleBegin() may return false to ignore the
         * rest of the gesture.
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            Log.i("tester", "onScaleBegin");
            return true;
        }

        /**
         * Responds to the end of a scale gesture. Reported by existing
         * pointers going up.
         * <p/>
         * Once a scale has ended, {@link ScaleGestureDetector#getFocusX()}
         * and {@link ScaleGestureDetector#getFocusY()} will return focal point
         * of the pointers remaining on the screen.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            mCameraSource.doZoom(detector.getScaleFactor());
            Log.i("tester", "onScaleEnd");
        }
    }

    static class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {
        private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;

        BarcodeTrackerFactory(GraphicOverlay<BarcodeGraphic> barcodeGraphicOverlay) {
            mGraphicOverlay = barcodeGraphicOverlay;
        }

        @Override
        public Tracker<Barcode> create(Barcode barcode) {
            BarcodeGraphic graphic = new BarcodeGraphic(mGraphicOverlay);
            return new BarcodeGraphicTracker(mGraphicOverlay, graphic);
        }
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = GlobalValue.mainActivity.getApplicationContext();

        // A barcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the barcode detection results, track the barcodes, and maintain
        // graphics for each barcode on screen.  The factory is used by the multi-processor to
        // create a separate tracker instance for each barcode.
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).build();
        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay);
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = GlobalValue.mainActivity.registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                //Toast.makeText(this, "ошибка", Toast.LENGTH_LONG).show();
                Log.w(TAG, "ошибка");
            }
        }

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode detector to detect small barcodes
        // at long distances.
        CameraSource.Builder builder = new CameraSource.Builder(GlobalValue.mainActivity.getApplicationContext(), barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)   //ВЫБОР КАМЕРЫ
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();
    }

}
