package ru.ithaus.ticket.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import ru.ithaus.ticket.other.MyCache;
import ru.pa_resultant.transportticket.R;

import ru.ithaus.ticket.animate.AnimateButtonUpDown;
import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.app.MyApp;
import ru.ithaus.ticket.modelApi.Auth;
import ru.ithaus.ticket.modelDB.DBDriver;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityLogIn extends AppCompatActivity {

    TextView textViewStart, textViewConnect;
    EditText editTexLogIn, editTextPsw;
    ProgressBar progressBar;

    ImageView imageViewStatus;

    private SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main_log_in);

        init();

        //TODO: Проверочка
        //throw new RuntimeException("Приветики ^_^");

    }

    /**
     * Инициализация
     */
    private void init() {

        mSettings = getSharedPreferences(GlobalValue.APP_PREFERENCES, Context.MODE_PRIVATE);
        GlobalValue.lastDate = "";
        if (mSettings.contains(GlobalValue.APP_PREFERENCES_LAST_DATE_CARD)) {
            GlobalValue.lastDate = mSettings.getString(GlobalValue.APP_PREFERENCES_LAST_DATE_CARD, "");
        }

        imageViewStatus = (ImageView) findViewById(R.id.imageViewStatus);
        imageViewStatus.setImageResource(R.drawable.ic_cast_connected_black_24dp);

        visibleErrorBlock1(View.INVISIBLE, "");
        visibleErrorBlock2(View.INVISIBLE);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        //Проверка на разрешения системы для Android 6+
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //вывод запроса разрешений
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.INTERNET,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.CAMERA
                    },
                    999);
        }

        editTexLogIn = (EditText) findViewById(R.id.editTexLogIn);
        editTextPsw = (EditText) findViewById(R.id.editTextPsw);

        textViewConnect = (TextView) findViewById(R.id.textViewConnect);
        textViewConnect.setVisibility(View.INVISIBLE);

        textViewStart = (TextView) findViewById(R.id.textViewStartButton);
        textViewStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                visibleErrorBlock1(View.INVISIBLE, "");
                visibleErrorBlock2(View.INVISIBLE);

                progressBar.setVisibility(View.VISIBLE);
                textViewConnect.setVisibility(View.VISIBLE);
                textViewStart.setVisibility(View.INVISIBLE);
                imageViewStatus.setImageResource(R.drawable.ic_cast_connected_black_24dp);

                GlobalValue.login = editTexLogIn.getText().toString();
                GlobalValue.password = editTextPsw.getText().toString();

                //Отправка запроса авторизации для backend
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("login", GlobalValue.login);
                parameters.put("password", GlobalValue.password);

                MyApp.getApiAuthPost().getData(parameters).enqueue(new Callback<Auth>() {
                    @Override
                    public void onResponse(Call<Auth> call, Response<Auth> response) {

                        Auth auth = response.body();  //Получение ответа авторизации

                        GlobalValue.offlineMode = false;

                        if (auth == null) {
                            visibleErrorBlock1(View.VISIBLE, "Неверный логин или пароль!");
                            Toast.makeText(MainActivityLogIn.this, "Неверный логин или пароль!", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                            textViewConnect.setVisibility(View.INVISIBLE);
                            textViewStart.setVisibility(View.VISIBLE);
                        } else {
                            Toast.makeText(MainActivityLogIn.this, "Добро пожаловать,\n" + auth.getUser(), Toast.LENGTH_SHORT).show();
                            GlobalValue.nameDriver = auth.getUser();
                            GlobalValue.token  = auth.getToken();
                            GlobalValue.userId = auth.getUser_id();

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<Auth> call, Throwable t) {
                        Toast.makeText(MainActivityLogIn.this, "ОШИБКА СЕТИ!!!", Toast.LENGTH_SHORT).show();
                        Log.i("tester", "error:   " + t.toString());
                        progressBar.setVisibility(View.INVISIBLE);
                        textViewConnect.setVisibility(View.INVISIBLE);
                        textViewStart.setVisibility(View.VISIBLE);
                        imageViewStatus.setImageResource(R.drawable.notserver);
                        visibleErrorBlock1(View.VISIBLE, "Internet отсутствует");
                        visibleErrorBlock2(View.VISIBLE);

                        GlobalValue.offlineMode = true;

                        //Работаем через cache
                        Realm realm = Realm.getDefaultInstance();
                        final RealmResults<DBDriver> result = realm.where(DBDriver.class)
                                .equalTo("login", GlobalValue.login)
                                .equalTo("password", GlobalValue.password)
                                .equalTo("isActive", 1)
                                .findAll();

                        if (result.size() > 0) {

                            GlobalValue.userId = result.get(0).getUser_id();
                            MyCache.addLogIn(GlobalValue.login, GlobalValue.password);  //Пишем кэш
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                });

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        /*if (requestCode == 999 && grantResults.length == 2) {

        }*/
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Управление видимостью блока ошибок №1
     */
    public void visibleErrorBlock1(int statusVisible, String info) {
        TextView textViewInfo1 = (TextView) findViewById(R.id.textViewInfo1);
        textViewInfo1.setVisibility(statusVisible);
        textViewInfo1.setText(info);
        ImageView imageViewInfo1 = (ImageView) findViewById(R.id.imageViewInfo1);
        imageViewInfo1.setVisibility(statusVisible);
        if (statusVisible == View.VISIBLE) {
            new AnimateButtonUpDown(textViewInfo1);
            new AnimateButtonUpDown(imageViewInfo1);
        }
    }

    /**
     * Управление видимостью блока ошибок №2
     */
    public void visibleErrorBlock2(int statusVisible) {
        TextView textViewInfo2 = (TextView) findViewById(R.id.textViewInfo2);
        textViewInfo2.setVisibility(statusVisible);
        ImageView imageViewInfo2 = (ImageView) findViewById(R.id.imageViewInfo2);
        imageViewInfo2.setVisibility(statusVisible);
        if (statusVisible == View.VISIBLE) {
            new AnimateButtonUpDown(textViewInfo2);
            new AnimateButtonUpDown(imageViewInfo2);
        }
    }
}
