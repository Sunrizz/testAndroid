package ru.ithaus.ticket.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ru.ithaus.ticket.animate.AnimateButtonUpDown;
import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelApi.Auth;
import ru.ithaus.ticket.modelDB.DBCache;
import ru.ithaus.ticket.modelDB.DBCards;
import ru.ithaus.ticket.other.MyGPS;
import ru.ithaus.ticket.other.SyncDataDB;
import ru.pa_resultant.transportticket.R;

import ru.ithaus.ticket.app.MyApp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    final int MIN_UPDATE_CARD=10;  //Интервал обновления билетов с сервера
    final int MIN_SEND_GPS = 5;    //Интервал отправки GPS данных на сервер

    DrawerLayout drawer;
    TextView textViewNameDriver;

    ImageView imageViewBurger;

    private LocationManager locationManager;

    FragmentScanCard fragmentScanCard;
    FragmentTransaction fragmentTransaction;
    boolean isStarted = false;

    TextView textViewInfo1;
    ImageView imageViewInfo1;

    TextView textViewInfo2;
    ImageView imageViewInfo2;

    ImageView imageViewInet;
    Timer mTimerUpdateFromServer = null;   //Таймер по которому приходит обновление от сервера

    Timer mTimerGps = null; //Таймер по которому считываем показания GPS для отправки на сервер, т.к.
    // нам надо получать данные GPS чаще чем раз в минуту, поэтому считываем все данные с GPS, вычисляем и накапливаем пройденный путь водителем
    // а вот точки GPS фиксируем раз в минуту с пройденным путем
//++++
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        realm = Realm.getDefaultInstance();

        init();
        new StartInitDrawerAndUpdate().execute();
    }

    private void init() {

        GlobalValue.mainActivity = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        imageViewInet = (ImageView) findViewById(R.id.imageViewInet);

        textViewNameDriver = (TextView) findViewById(R.id.textViewNameDriver);
        textViewNameDriver.setText(GlobalValue.nameDriver);

        fragmentScanCard = new FragmentScanCard();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frgmCont, fragmentScanCard);
        fragmentTransaction.commit();

        imageViewBurger = (ImageView) findViewById(R.id.imageViewMenu);
        imageViewBurger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.END);
            }
        });

        textViewInfo1 = (TextView) findViewById(R.id.textViewInfo1);
        imageViewInfo1 = (ImageView) findViewById(R.id.imageViewInfo1);
        showHideInfo1("", View.INVISIBLE);

        textViewInfo2 = (TextView) findViewById(R.id.textViewInfo2);
        imageViewInfo2 = (ImageView) findViewById(R.id.imageViewInfo2);
        showHideInfo2("", View.INVISIBLE);

        //Подключаем GPS
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabledGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!enabledGPS) {
            showHideInfo2("Включите GPS!", View.VISIBLE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000 * 10, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 10, 10, locationListener);

        changeStatusInternet(!GlobalValue.offlineMode);
        //Установим таймер для обновления
        if (mTimerUpdateFromServer == null) {
            mTimerUpdateFromServer = new Timer();
            MyTimerTask mMyTimerTask = new MyTimerTask();
            mTimerUpdateFromServer.schedule(mMyTimerTask, 1000, (1000 * 60) * MIN_UPDATE_CARD);    //Связь с сервером по таймеру 10мин
        }

        //По этому таймеру отправляем показания GPS
        if (mTimerGps == null) {
            mTimerGps = new Timer();
            MyTimerTaskSendToServer myTimerTaskSendToServer = new MyTimerTaskSendToServer();
            mTimerGps.schedule(myTimerTaskSendToServer, 10000, (1000 * 60)*MIN_SEND_GPS);       //Связь с сервером по таймеру 5мин
        }

    }

    /**
     * Обновление данных с сервера
     */
    class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                    "dd:MMMM:yyyy HH:mm:ss a", Locale.getDefault());
            final String strDate = simpleDateFormat.format(calendar.getTime());
            Log.i("tester", "run: SyncDataDB.updateAll() " + strDate);

            SyncDataDB.updateAll();

           /* runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(MainActivity.this, "Update "+strDate, Toast.LENGTH_SHORT).show();
                    //SyncDataDB.updateAll();
                }
            });*/
        }
    }

    /**
     * Отправяет GPS точки водителя и КЭШ
     * Эта отправка данных на сервер, должна вызываться по таймеру
     * Суть - у нас есть вызовы методов MyGPS - если были ошибки при отправке на сервер, то эти
     * данные записывались в базу DBCache поле sync=0 означает что данные не отправлены
     * вот в этом классе мы и отправим все на сервер
     */
    class MyTimerTaskSendToServer extends TimerTask {

        @Override
        public void run() {

            //Попытка отправить координаты
            MyGPS.trackDataOnServer(GlobalValue.getCurrentDateFormatUser(), GlobalValue.getRoundGPS(GlobalValue.lat), GlobalValue.getRoundGPS(GlobalValue.lon), GlobalValue.mileage, GlobalValue.userId, true, null);
            GlobalValue.mileage = 0; //Обнулим, накопление происходит в слушателе GPS

            //проверка на наличие интернета и вывод на активити блока, что нет интернета и т.д.
            String cs = Context.CONNECTIVITY_SERVICE;
            ConnectivityManager cm = (ConnectivityManager)
                    getSystemService(cs);
            if (cm.getActiveNetworkInfo() == null) {
                GlobalValue.offlineMode = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        GlobalValue.mainActivity.showHideInfo1("Нет интернета!", View.VISIBLE);
                        GlobalValue.mainActivity.changeStatusInternet(false);
                    }
                });
                return;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Отправка GPS!", Toast.LENGTH_SHORT).show();
                    GlobalValue.mainActivity.showHideInfo1("Нет интернета!", View.INVISIBLE);
                    GlobalValue.mainActivity.changeStatusInternet(true);
                }
            });


            //Мы здесь - значит инет у нас есть
            //Проверим наличие записей в КЭШе sync=0
            //и если там есть таковые - попробуем их отправить на сервер

            Realm realm1 = Realm.getDefaultInstance();
            final RealmResults<DBCache> result = realm1.where(DBCache.class).equalTo("sync", 0).findAll();

            try {
                for (int i = 0; i < result.size(); i++) {
                    final int position = i;

                    //В КЭШе есть getNameCommand() с имененм не отправленной команды и со всеми параметрами
                    //Обработаем эти команды

                    //1 Авторизация
                    if (result.get(i).getNameCommand().equals("auth")) {
                        //Если не было авторизации
                        //Отправка запроса авторизации для backend
                        Map<String, String> parameters = new HashMap<String, String>();
                        parameters.put("login", GlobalValue.login);
                        parameters.put("password", GlobalValue.password);

                        MyApp.getApiAuthPost().getData(parameters).enqueue(new Callback<Auth>() {
                            @Override
                            public void onResponse(Call<Auth> call, Response<Auth> response) {
                                Auth auth = response.body();  //Получение ответа авторизации
                                GlobalValue.offlineMode = false;
                                if (auth != null) {
                                    if (GlobalValue.token == null) {
                                        GlobalValue.token = auth.getToken();
                                        GlobalValue.userId = auth.getUser_id();
                                    }
                                }


                                Realm realmTask = Realm.getDefaultInstance();
                                try {
                                    realmTask.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            result.get(position).setSync(1);
                                            realm.insertOrUpdate(result.get(position));
                                        }
                                    });
                                } catch (Exception e) {

                                } finally {
                                    realmTask.close();
                                }

                            }

                            @Override
                            public void onFailure(Call<Auth> call, Throwable t) {
                                GlobalValue.offlineMode = true;
                            }
                        });
                    }

                    if (result.get(i).getNameCommand().equals("trackStart")) {
                        //Последний параметр всегда false т.к. мы не хотим кэшировать то что закэшировано при ошибке
                        MyGPS.trackSatrtOnServer(result.get(i).getDate(), result.get(i).getLat(), result.get(i).getLon(), result.get(i).getUserId(), false, result.get(i).getDate());
                    }

                    if (result.get(i).getNameCommand().equals("trackData")) {
                        MyGPS.trackDataOnServer(result.get(i).getDate(), result.get(i).getLat(), result.get(i).getLon(), result.get(i).getMileage(), result.get(i).getUserId(), false, result.get(i).getDate());
                    }

                    if (result.get(i).getNameCommand().equals("trackEnd")) {
                        MyGPS.trackEndOnServer(result.get(i).getDate(), result.get(i).getLat(), result.get(i).getLon(), result.get(i).getUserId(), false, result.get(i).getDate());
                    }

                    if (result.get(i).getNameCommand().equals("tripStart")) {
                        MyGPS.tripStartOnServer(result.get(i).getKod(), result.get(i).getDate(), result.get(i).getLat(), result.get(i).getLon(), result.get(i).getPassengerQty(), result.get(i).getUserId(), false, result.get(i).getDate());
                    }
                }
            }finally {
                realm1.close();
            }

            //Удалим из базы кэша все отправленные записи
            Realm realmTask = Realm.getDefaultInstance();
            try {
                realmTask.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<DBCache> result = realm.where(DBCache.class).equalTo("sync", 1).findAll();
                        result.deleteAllFromRealm();
                    }
                });
            }catch (Exception e){

            }finally {
                realmTask.close();
            }

        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Intent intent = new Intent(getApplicationContext(), MainActivityUpdate.class);
            startActivity(intent);
        }
    };

    public void openUpdateActivity() {
        handler.sendEmptyMessage(0);
    }

    public void changeStatusInternet(boolean internetActive) {
        Message msg = new Message();

        if (!internetActive) {
            msg.arg1 = 0;
        } else {
            msg.arg1 = 1;
        }

        handlerChangeStatusInternet.sendMessage(msg);

    }

    Handler handlerChangeStatusInternet = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.arg1 == 0) {
                imageViewInet.setImageResource(R.drawable.notserver);
            } else {
                imageViewInet.setImageResource(R.drawable.ic_cast_connected_black_24dp);
            }
        }
    };

    public void showHideInfo1(String strInfo, int visible) {
        textViewInfo1.setText(strInfo);
        textViewInfo1.setVisibility(visible);
        imageViewInfo1.setVisibility(visible);
    }

    public void showHideInfo2(String strInfo, int visible) {
        textViewInfo2.setText(strInfo);
        textViewInfo2.setVisibility(visible);
        imageViewInfo2.setVisibility(visible);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                Toast.makeText(this, "Используйте правое меню - пункт Выход ", Toast.LENGTH_SHORT).show();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class StartInitDrawerAndUpdate extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... arg) {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final String result) {

            final TextView textViewInfoCard = ((TextView) drawer.findViewById(R.id.textViewInfoCard));
            if (textViewInfoCard != null) {
                textViewInfoCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AnimateButtonUpDown(textViewInfoCard);
                        GlobalValue.checkCardMode = true;
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        if (drawer.isDrawerOpen(GravityCompat.END)) {
                            drawer.closeDrawer(GravityCompat.END);
                        }
                        getSupportFragmentManager().popBackStack();
                        getSupportFragmentManager().popBackStack();
                        Toast.makeText(MainActivity.this, textViewInfoCard.getText().toString() + " для возврата выберите \"К поездке\"", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            final TextView textViewBlock = ((TextView) drawer.findViewById(R.id.textViewBlock));
            if (textViewBlock != null) {
                textViewBlock.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AnimateButtonUpDown(textViewBlock);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        if (drawer.isDrawerOpen(GravityCompat.END)) {
                            drawer.closeDrawer(GravityCompat.END);
                        }
                        Toast.makeText(MainActivity.this, textViewBlock.getText().toString(), Toast.LENGTH_SHORT).show();
                        onOpenFragmenBlockCard();
                    }
                });
            }

            final TextView textViewToWay = ((TextView) drawer.findViewById(R.id.textViewToWay));
            if (textViewToWay != null) {
                textViewToWay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AnimateButtonUpDown(textViewToWay);
                        GlobalValue.checkCardMode = false;
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        if (drawer.isDrawerOpen(GravityCompat.END)) {
                            drawer.closeDrawer(GravityCompat.END);
                        }
                        getSupportFragmentManager().popBackStack();
                        getSupportFragmentManager().popBackStack();
                        Toast.makeText(MainActivity.this, textViewToWay.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            final TextView textViewUpd = ((TextView) drawer.findViewById(R.id.textViewUpd));
            if (textViewUpd != null) {
                textViewUpd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AnimateButtonUpDown(textViewUpd);
                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        if (drawer.isDrawerOpen(GravityCompat.END)) {
                            drawer.closeDrawer(GravityCompat.END);
                        }
                        SyncDataDB.updateAll();
                        Toast.makeText(MainActivity.this, textViewUpd.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            TextView textViewExit = ((TextView) drawer.findViewById(R.id.textViewExit));
            if (textViewExit != null) {
                textViewExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(MainActivity.this, "До свидания!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }
        }
    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();
        //fragmentScanCard.startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        /*if (fragmentScanCard.mPreview != null) {
            fragmentScanCard.mPreview.stop();
        }*/
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*if (fragmentScanCard.mPreview != null) {
            fragmentScanCard.mPreview.release();
        }*/

        MyGPS.trackEndOnServer(GlobalValue.getCurrentDateFormatUser(), GlobalValue.getRoundGPS(GlobalValue.lat), GlobalValue.getRoundGPS(GlobalValue.lon), GlobalValue.userId, true, null);
        if (mTimerUpdateFromServer != null) {
            mTimerUpdateFromServer.cancel();
        }
        if (mTimerGps != null) {
            mTimerGps.cancel();
        }

        this.realm.close();
    }

    @Override
    public void onStop() {
        super.onStop();
        //realm.close();
    }

    public void stopScaner() {
        if (fragmentScanCard.mPreview != null) {
            fragmentScanCard.mPreview.release();
        }
    }

    public void startScaner() {
        if (fragmentScanCard.mPreview != null) {
            fragmentScanCard.startCameraSource();
        }
    }

    /**
     * Вызывается при сканировании или вводе кода с клавиатуры
     * @param scanCode  код карты
     */
    public void onDataFromScan(String scanCode) {

        fragmentScanCard.showHideInfo("", View.INVISIBLE);

        //Сканеру делаем "инженерные" команды
        if (scanCode.equals("test cache")) {  //открывает на просмотр кэш
            onOpenFragmenCache();
            return;
        }

        if (scanCode.equals("clear cache")) {  //очистка кэша
            //Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<DBCache> result = realm.where(DBCache.class).findAll();
                    result.deleteAllFromRealm();
                }
            });
            return;
        }

        if (scanCode.equals("open all 999-0")) {  //открывает на просмотр все билеты
            onOpenFragmenAllDebugCard();
            return;
        }

        final MediaPlayer mPlayer = MediaPlayer.create(MainActivity.this, R.raw.scan);
        mPlayer.start();
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mPlayer.release();
            }
        });

        //Найдем билет в базе
        //Realm realm = Realm.getDefaultInstance();
        final RealmResults<DBCards> result = realm.where(DBCards.class)
                .equalTo("kod", scanCode)
                .findAll();

        if (GlobalValue.checkCardMode == false) {

            if (result.size() > 0) {

                if (result.size() > 0) {
                    if (result.get(0).getIsBlocked() == 1) {
                        //Проверим на блокировку
                        fragmentScanCard.showHideInfo("Карта:" + scanCode + " ЗАБЛОКИРОВАНА!", View.VISIBLE);
                        return;
                    }
                }

                GlobalValue.cardType = result.get(0).getCardType();    //Запишем данные выбора карты в GlobalValue
                GlobalValue.cardDateEnd = result.get(0).getDateEnd();  //далее эти данные используем для отображения на фрагментах

                if ( GlobalValue.cardType == 2) {
                    //Проверим количество поездок и дату end
                    if ((result.get(0).getMaxTripQty() - result.get(0).getTripQty()) < 1) {
                        fragmentScanCard.showHideInfo("На карте\nНЕТ ПОЕЗДОК", View.VISIBLE);
                        return;
                    }
                    if(result.get(0).getDateEnd()!=null) {
                        Date d1 = GlobalValue.parseDateFromString(result.get(0).getDateEnd());
                        if(d1.before(new Date())){
                            fragmentScanCard.showHideInfo("Карта просрочена", View.VISIBLE);
                            return;
                        }
                    }
                    GlobalValue.ammountWay = result.get(0).getMaxTripQty() - result.get(0).getTripQty();
                }

                if ( GlobalValue.cardType == 1) {
                    //Проверим дату end
                    GlobalValue.ammountWay = -99;
                    Date d1 = GlobalValue.parseDateFromString(result.get(0).getDateEnd());
                    if(d1!=null) {
                        if (d1.before(new Date())) {
                            fragmentScanCard.showHideInfo("Карта просрочена", View.VISIBLE);
                            return;
                        }
                    }

                    d1 = GlobalValue.parseDateFromString(result.get(0).getDateStart());
                    if(d1!=null) {
                        if (d1.after(new Date())) {
                            fragmentScanCard.showHideInfo("Карта пока\nнедействительна", View.VISIBLE);
                            return;
                        }
                    }

                }

                GlobalValue.cardNum = scanCode;
                fragmentScanCard.editTexNumCard.setText(scanCode);

                openFragmentSelectPass(true);

            } else {
                fragmentScanCard.showHideInfo("Билет " + scanCode + " не найден!", View.VISIBLE);
                return;
                //Toast.makeText(MainActivity.this, "Билет не найден!", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (result.size() > 0) {
                GlobalValue.cardNum = scanCode;
                GlobalValue.ammountWay = result.get(0).getMaxTripQty() - result.get(0).getTripQty();
                GlobalValue.cardDateEnd = result.get(0).getDateEnd();
                if (result.get(0).getIsBlocked() == 1) {
                    GlobalValue.cardStatus = "Заблокирована";
                } else {
                    GlobalValue.cardStatus = "Активна";
                }
                onOpenFragmenCheckInfoCard();
            } else {
                fragmentScanCard.showHideInfo("Билет " + scanCode + " не найден!", View.VISIBLE);
                return;
            }
        }

        fragmentScanCard.editTexNumCard.setText("");

    }

    /**
     * Открывает фрагмент выбора кол-ва пассажиров
     * @param backStack если true то попадает в стек кнопки back
     */
    public void openFragmentSelectPass(boolean backStack) {
        FragmentSelectPass fragmentSelectPass = new FragmentSelectPass();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frgmCont, fragmentSelectPass);
        if (backStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    /**
     * открывает фрагмент хорошего пути
     */
    public void onOpenFragmenGoodWay() {
        FragmentGoodWay fragmentGoodWay = new FragmentGoodWay();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frgmCont, fragmentGoodWay);
        fragmentTransaction.commit();
    }

    /**
     * открывает фрагмент проверка карты
     */
    public void onOpenFragmenCheckInfoCard() {
        FragmentCheckInfoCard fragmentCheckInfoCard = new FragmentCheckInfoCard();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frgmCont, fragmentCheckInfoCard);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * открывает фрагмент порсмотра кэша, доступно через команду test cache со сканера
     */
    public void onOpenFragmenCache() {
        FragmentCache fragmentCache = new FragmentCache();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frgmCont, fragmentCache);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * открывает фрагмент заблокированных карт
     */
    public void onOpenFragmenBlockCard() {
        FragmentBlockCard fragmentBlockCard = new FragmentBlockCard();
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frgmCont, fragmentBlockCard);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * открывает фрагмент всех карт, доступно по команде сканера open all 999-0
     */
    public void onOpenFragmenAllDebugCard() {
        FragmentBlockCard fragmentBlockCard = new FragmentBlockCard();
        fragmentBlockCard.openAll=true;
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frgmCont, fragmentBlockCard);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    /**
     * GPS слушатель
     */
    public LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            //Кэшируем координты начала поездки водителя

            if (isStarted == false) {
                MyGPS.trackSatrtOnServer(GlobalValue.getCurrentDateFormatUser(), GlobalValue.getRoundGPS(GlobalValue.lat), GlobalValue.getRoundGPS(GlobalValue.lon), GlobalValue.userId, true, null);
            } else {
                if(GlobalValue.lat>0) {
                    GlobalValue.mileage = GlobalValue.mileage + (int) MyGPS.getDistance(location.getLatitude(), location.getLongitude(), GlobalValue.lat, GlobalValue.lon);
                }
            }
            isStarted = true;
            GlobalValue.lat = location.getLatitude();
            GlobalValue.lon = location.getLongitude();

            showHideInfo2("Включите GPS!", View.INVISIBLE);
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

}
