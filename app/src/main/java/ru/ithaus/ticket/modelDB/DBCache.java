package ru.ithaus.ticket.modelDB;

import io.realm.RealmObject;

/** КЭШ
 *  База данных для всех данных которые отправляются кэшем
 */

public class DBCache extends RealmObject {

    private int trackId;
    private int mileage;
    private String lat;
    private String lon;
    private String date;
    private int sync;   //0 - не отправлено на сервер, 1 - отправлено на сервер
    private String nameCommand;
    private String login;
    private String password;
    private String passengerQty;
    private int userId;
    private String kod;

    public DBCache() {
    }

    public int getTrackId() {
        return trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getNameCommand() {
        return nameCommand;
    }

    public void setNameCommand(String nameCommand) {
        this.nameCommand = nameCommand;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassengerQty() {
        return passengerQty;
    }

    public void setPassengerQty(String passengerQty) {
        this.passengerQty = passengerQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }
}
