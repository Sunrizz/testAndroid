package ru.ithaus.ticket.modelDB;

import io.realm.RealmObject;

/**База данных Настройки
 *
 */

public class DBSettings extends RealmObject {

    String lastDate;

    public DBSettings() {
    }

    public String getLastDate() {
        return lastDate;
    }

    public void setLastDate(String lastDate) {
        this.lastDate = lastDate;
    }
}
