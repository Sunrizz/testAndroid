package ru.ithaus.ticket.modelDB;

import io.realm.RealmObject;

/**База данных водителей
 *
 */

public class DBDriver extends RealmObject {

    int user_id;
    String login;
    String password;
    String fullname;
    int isActive;
    String lastToken;
    int lastDateUpdate;

    public DBDriver() {

    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getLastToken() {
        return lastToken;
    }

    public void setLastToken(String lastToken) {
        this.lastToken = lastToken;
    }

    public int getLastDateUpdate() {
        return lastDateUpdate;
    }

    public void setLastDateUpdate(int lastDateUpdate) {
        this.lastDateUpdate = lastDateUpdate;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
