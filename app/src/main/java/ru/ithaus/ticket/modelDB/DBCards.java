package ru.ithaus.ticket.modelDB;

import io.realm.RealmObject;

/**База данных билетов
 *
 */

public class DBCards extends RealmObject {

    private String kod;
    private int cardType;
    private int isBlocked;
    private int tripQty;
    private int maxTripQty;
    private String name;
    private String dateActivate;
    private String dateStart;
    private String dateEnd;
    private String updated_at;

    public DBCards() {
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    public int getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(int isBlocked) {
        this.isBlocked = isBlocked;
    }

    public int getTripQty() {
        return tripQty;
    }

    public void setTripQty(int tripQty) {
        this.tripQty = tripQty;
    }

    public int getMaxTripQty() {
        return maxTripQty;
    }

    public void setMaxTripQty(int maxTripQty) {
        this.maxTripQty = maxTripQty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateActivate() {
        return dateActivate;
    }

    public void setDateActivate(String dateActivate) {
        this.dateActivate = dateActivate;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
