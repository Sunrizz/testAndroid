package ru.ithaus.ticket.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import ru.pa_resultant.transportticket.R;


/**
 * Created by us on 22.07.2017.
 */

public class FontableTextView extends TextView {
    public FontableTextView(Context context) {
        super(context);
    }

    public FontableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.ru_pa_resultant_transportticket_view_FontableTextView,
                R.styleable.ru_pa_resultant_transportticket_view_FontableTextView_font);
    }

    public FontableTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        UiUtil.setCustomFont(this, context, attrs,
                R.styleable.ru_pa_resultant_transportticket_view_FontableTextView,
                R.styleable.ru_pa_resultant_transportticket_view_FontableTextView_font);
    }
}
