package ru.ithaus.ticket.modelApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by us on 21.07.2017.
 */

public class Auth {

    @SerializedName("token")
    @Expose
    String token;

    @SerializedName("user")
    @Expose
    String user;

    @SerializedName("user_id")
    @Expose
    int user_id;

    @SerializedName("date")
    @Expose
    String date;

    @SerializedName("timestamp")
    @Expose
    int timestamp;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
