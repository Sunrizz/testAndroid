package ru.ithaus.ticket.modelApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by us on 21.07.2017.
 */

public class TrackStart {

    @SerializedName("error")
    @Expose
    int error;

    @SerializedName("data")
    @Expose
    DataTrackStart data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public DataTrackStart getData() {
        return data;
    }

    public void setData(DataTrackStart data) {
        this.data = data;
    }

    public class DataTrackStart {
        @SerializedName("trackId")
        @Expose
        int trackId;

        public int getTrackId() {
            return trackId;
        }

        public void setTrackId(int trackId) {
            this.trackId = trackId;
        }
    }

}
