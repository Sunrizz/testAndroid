package ru.ithaus.ticket.modelApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by us on 21.07.2017.
 */

public class Driver {

    @SerializedName("error")
    @Expose
    int error;

    @SerializedName("data")
    @Expose
    List<DataDriver> data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public List<DataDriver> getData() {
        return data;
    }

    public void setData(List<DataDriver> data) {
        this.data = data;
    }

    public class DataDriver {

        @SerializedName("user_id")
        @Expose
        int user_id;

        @SerializedName("login")
        @Expose
        String login;

        @SerializedName("password")
        @Expose
        String password;

        @SerializedName("fullname")
        @Expose
        String fullname;

        @SerializedName("isActive")
        @Expose
        int isActive;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getFullname() {
            return fullname;
        }

        public void setFullname(String fullname) {
            this.fullname = fullname;
        }

        public int getIsActive() {
            return isActive;
        }

        public void setIsActive(int isActive) {
            this.isActive = isActive;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }
    }

}
