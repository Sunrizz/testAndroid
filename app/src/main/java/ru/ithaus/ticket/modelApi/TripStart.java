package ru.ithaus.ticket.modelApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by us on 21.07.2017.
 */

public class TripStart {

    @SerializedName("error")
    @Expose
    int error;

    @SerializedName("data")
    @Expose
    DataTripStart data;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public DataTripStart getData() {
        return data;
    }

    public void setData(DataTripStart data) {
        this.data = data;
    }

    public class DataTripStart {
        @SerializedName("tripId")
        @Expose
        String tripId;

        public String getTripId() {
            return tripId;
        }

        public void setTripId(String tripId) {
            this.tripId = tripId;
        }
    }
}
