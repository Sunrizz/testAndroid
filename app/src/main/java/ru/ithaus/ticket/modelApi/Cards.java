package ru.ithaus.ticket.modelApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by us on 25.07.2017.
 */

public class Cards {

    @SerializedName("error")
    @Expose
    int error;

    @SerializedName("data")
    @Expose
    CerdsData CardsData;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public CerdsData getCardsData() {
        return CardsData;
    }

    public void setCardsData(CerdsData cardsData) {
        CardsData = cardsData;
    }

    public class CerdsData {
        @SerializedName("lastDate")
        @Expose
        String lastDate;

        @SerializedName("data")
        @Expose
        List<DataCards> data;

        public String getLastDate() {
            return lastDate;
        }

        public void setLastDate(String lastDate) {
            this.lastDate = lastDate;
        }

        public List<DataCards> getData() {
            return data;
        }

        public void setData(List<DataCards> data) {
            this.data = data;
        }

        public class DataCards {

            @SerializedName("kod")
            @Expose
            private String kod;

            @SerializedName("cardType")
            @Expose
            private int cardType;

            @SerializedName("isBlocked")
            @Expose
            private int isBlocked;

            @SerializedName("tripQty")
            @Expose
            private int tripQty;

            @SerializedName("maxTripQty")
            @Expose
            private int maxTripQty;

            @SerializedName("name")
            @Expose
            private String name;

            @SerializedName("dateActivate")
            @Expose
            private String dateActivate;

            @SerializedName("dateStart")
            @Expose
            private String dateStart;

            @SerializedName("dateEnd")
            @Expose
            private String dateEnd;

            @SerializedName("updated_at")
            @Expose
            private String updated_at;

            public String getKod() {
                return kod;
            }

            public void setKod(String kod) {
                this.kod = kod;
            }

            public int getCardType() {
                return cardType;
            }

            public void setCardType(int cardType) {
                this.cardType = cardType;
            }

            public int getIsBlocked() {
                return isBlocked;
            }

            public void setIsBlocked(int isBlocked) {
                this.isBlocked = isBlocked;
            }

            public int getTripQty() {
                return tripQty;
            }

            public void setTripQty(int tripQty) {
                this.tripQty = tripQty;
            }

            public int getMaxTripQty() {
                return maxTripQty;
            }

            public void setMaxTripQty(int maxTripQty) {
                this.maxTripQty = maxTripQty;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDateActivate() {
                return dateActivate;
            }

            public void setDateActivate(String dateActivate) {
                this.dateActivate = dateActivate;
            }

            public String getDateStart() {
                return dateStart;
            }

            public void setDateStart(String dateStart) {
                this.dateStart = dateStart;
            }

            public String getDateEnd() {
                return dateEnd;
            }

            public void setDateEnd(String dateEnd) {
                this.dateEnd = dateEnd;
            }

            public String getUpdated_at() {
                return updated_at;
            }

            public void setUpdated_at(String updated_at) {
                this.updated_at = updated_at;
            }
        }
    }

}
