package ru.ithaus.ticket.other;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelApi.Cards;
import ru.ithaus.ticket.modelApi.Driver;
import ru.ithaus.ticket.modelDB.DBCards;
import ru.ithaus.ticket.app.MyApp;
import ru.ithaus.ticket.modelDB.DBDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Класс предназначен для работы с базой данных
 * и синхронизацией данных
 * Created by us on 21.07.2017.
 */

public class SyncDataDB {

    public static final int delayTimeForClose = 1000;

    /**
     * Обновляет в базе список водителей
     */
    public static void updateDriver() {

        if(GlobalValue.mainActivityUpdate!=null) {
            GlobalValue.mainActivityUpdate.outInfo("Обновление базы водителей");
        }

        MyApp.getApiDriverGet().getData(GlobalValue.bearerForAuth + GlobalValue.token).enqueue(new Callback<Driver>() {
            @Override
            public void onResponse(Call<Driver> call, Response<Driver> response) {
                GlobalValue.offlineMode = false;
                final Driver driverFromServer = response.body();  //Получение ответа со списком водителей

                GlobalValue.mainActivity.changeStatusInternet(true);
                GlobalValue.mainActivity.showHideInfo1("", View.INVISIBLE);

                if (driverFromServer == null) {
                    //Toast.makeText(GlobalValue.mainActivity, "Ошибка синхронизации с сервером!", Toast.LENGTH_SHORT).show();
                    GlobalValue.mainActivity.showHideInfo1("Ошибка синхронизации с сервером!", View.VISIBLE);
                    return;
                }

                //Из базы удалим всех водителей
                Realm realm = Realm.getDefaultInstance();
                try {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<DBDriver> result = realm.where(DBDriver.class).findAll();
                            result.deleteAllFromRealm();
                        }
                    });
                } finally {
                    realm.close();
                }

                final Realm realm2 = Realm.getDefaultInstance();
                try {
                    //Запишем в базу новые данные о водителях, полученные от сервера
                    for (int i = 0; i < driverFromServer.getData().size(); i++) {
                        final int position = i;

                        realm2.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                DBDriver dbDriver;
                                dbDriver = realm.createObject(DBDriver.class);
                                dbDriver.setUser_id(driverFromServer.getData().get(position).getUser_id());
                                dbDriver.setLogin(driverFromServer.getData().get(position).getLogin());
                                dbDriver.setPassword(driverFromServer.getData().get(position).getPassword());
                                dbDriver.setFullname(driverFromServer.getData().get(position).getFullname());
                                dbDriver.setIsActive(driverFromServer.getData().get(position).getIsActive());
                                realm2.insertOrUpdate(dbDriver);
                            }
                        });
                    }
                } finally {
                    realm2.close();
                }

            }

            @Override
            public void onFailure(Call<Driver> call, Throwable t) {
                GlobalValue.offlineMode = true;
                //Toast.makeText(GlobalValue.mainActivity, "1 ОШИБКА СЕТИ!!!", Toast.LENGTH_SHORT).show();
                GlobalValue.mainActivity.showHideInfo1("Нет интернета!", View.VISIBLE);
                GlobalValue.mainActivity.changeStatusInternet(false);
            }
        });
    }

    /**
     * Обновляет в базе список билетов
     */
    public static void updateCard() {

        if(GlobalValue.mainActivityUpdate!=null) {
            GlobalValue.mainActivityUpdate.outInfo("Обновление базы билетов, может загружаться более 5 мин");
        }

        if(GlobalValue.lastDate=="") {
            MyApp.getApiCardsDataGet().getData(GlobalValue.bearerForAuth + GlobalValue.token).enqueue(new Callback<Cards>() {
                @Override
                public void onResponse(Call<Cards> call, Response<Cards> response) {
                    saveCard(response);
                }

                @Override
                public void onFailure(Call<Cards> call, Throwable t) {
                    GlobalValue.offlineMode = true;
                    GlobalValue.processUpdate = false;

                    closeUpdateActivity(delayTimeForClose);

                    //Toast.makeText(GlobalValue.mainActivity, "2 ОШИБКА СЕТИ!!!", Toast.LENGTH_SHORT).show();
                    GlobalValue.mainActivity.showHideInfo1("Нет интернета!", View.VISIBLE);
                    GlobalValue.mainActivity.changeStatusInternet(false);
                }
            });
        }else {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("lastDate", GlobalValue.lastDate);
            MyApp.getApiCardsDataLastDateGet().getData(parameters, GlobalValue.bearerForAuth + GlobalValue.token).enqueue(new Callback<Cards>() {
                @Override
                public void onResponse(Call<Cards> call, Response<Cards> response) {
                    saveCard(response);
                }

                @Override
                public void onFailure(Call<Cards> call, Throwable t) {
                    GlobalValue.offlineMode = true;
                    GlobalValue.processUpdate = false;

                    closeUpdateActivity(delayTimeForClose);

                    //Toast.makeText(GlobalValue.mainActivity, "2 ОШИБКА СЕТИ!!!", Toast.LENGTH_SHORT).show();
                    GlobalValue.mainActivity.showHideInfo1("Нет интернета!", View.VISIBLE);
                    GlobalValue.mainActivity.changeStatusInternet(false);
                }
            });
        }
    }

    private static void saveCard(Response<Cards> response) {
        GlobalValue.processUpdate = false;
        GlobalValue.offlineMode = false;
        final Cards cards = response.body();  //Получение ответа со списком билетов

        GlobalValue.mainActivity.changeStatusInternet(true);
        GlobalValue.mainActivity.showHideInfo1("", View.INVISIBLE);

        if (cards == null) {
            //Toast.makeText(GlobalValue.mainActivity, "Ошибка синхронизации с сервером!", Toast.LENGTH_SHORT).show();
            GlobalValue.mainActivity.showHideInfo1("Ошибка синхронизации с сервером!", View.VISIBLE);
            if (GlobalValue.mainActivityUpdate != null) {
                GlobalValue.mainActivityUpdate.finish();
            }
            return;
        }

        SharedPreferences mSettings;
        GlobalValue.lastDate = cards.getCardsData().getLastDate();
        mSettings = GlobalValue.mainActivity.getSharedPreferences(GlobalValue.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(GlobalValue.APP_PREFERENCES_LAST_DATE_CARD, GlobalValue.lastDate);
        editor.apply();

        if(GlobalValue.mainActivityUpdate!=null) {
            GlobalValue.mainActivityUpdate.outInfo("Обновление базы билетов, "+cards.getCardsData().getData().size()+" шт, \nзагрузка может загружаться более 5 мин");
        }

        Log.i("tester", "Чтение билетов с сервера");

        for (int i = 0; i < cards.getCardsData().getData().size(); i++) {

            final int position = i;
            final Realm realmCard = Realm.getDefaultInstance();

            try {
                //Найдем билет в базе
                final RealmResults<DBCards> result = realmCard.where(DBCards.class)
                        .equalTo("kod", cards.getCardsData().getData().get(i).getKod())
                        .equalTo("cardType", cards.getCardsData().getData().get(i).getCardType())
                        .findAll();

                if (result.size() > 0) {
                    //Если запись в БД о билете есть, то обновим её серверными данными
                    realmCard.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            result.get(0).setIsBlocked(cards.getCardsData().getData().get(position).getIsBlocked());
                            result.get(0).setTripQty(cards.getCardsData().getData().get(position).getTripQty());
                            result.get(0).setMaxTripQty(cards.getCardsData().getData().get(position).getMaxTripQty());
                            result.get(0).setDateActivate(cards.getCardsData().getData().get(position).getDateActivate());
                            result.get(0).setDateStart(cards.getCardsData().getData().get(position).getDateStart());
                            result.get(0).setDateEnd(cards.getCardsData().getData().get(position).getDateEnd());
                            result.get(0).setUpdated_at(cards.getCardsData().getData().get(position).getUpdated_at());
                            realmCard.insertOrUpdate(result.get(0));
                            Log.i("tester", "Update: " + cards.getCardsData().getData().get(position).getKod() + "  " + cards.getCardsData().getData().get(position).getName() + " " + cards.getCardsData().getData().get(position).getDateStart());
                        }
                    });
                } else {
                    //Если запись в БД о билете отсутствует, то создаим ее
                    realmCard.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            DBCards dbCards = realmCard.createObject(DBCards.class);
                            dbCards.setKod(cards.getCardsData().getData().get(position).getKod());
                            dbCards.setName(cards.getCardsData().getData().get(position).getName());
                            dbCards.setCardType(cards.getCardsData().getData().get(position).getCardType());
                            dbCards.setIsBlocked(cards.getCardsData().getData().get(position).getIsBlocked());
                            dbCards.setTripQty(cards.getCardsData().getData().get(position).getTripQty());
                            dbCards.setMaxTripQty(cards.getCardsData().getData().get(position).getMaxTripQty());
                            dbCards.setDateActivate(cards.getCardsData().getData().get(position).getDateActivate());
                            dbCards.setDateStart(cards.getCardsData().getData().get(position).getDateStart());
                            dbCards.setDateEnd(cards.getCardsData().getData().get(position).getDateEnd());
                            dbCards.setUpdated_at(cards.getCardsData().getData().get(position).getUpdated_at());
                            realmCard.insertOrUpdate(dbCards);

                            Log.i("tester", "NEW: " + cards.getCardsData().getData().get(position).getKod() + "  " + cards.getCardsData().getData().get(position).getName() + " " + cards.getCardsData().getData().get(position).getDateStart());
                        }
                    });
                }
            } catch (Exception e) {

            } finally {
                realmCard.close();
                Log.i("tester", "REALM CLOSE");
            }
        }


        Toast.makeText(GlobalValue.mainActivity, "База обновлена!", Toast.LENGTH_SHORT).show();
        GlobalValue.mainActivity.changeStatusInternet(true);

        if (GlobalValue.mainActivityUpdate != null) {
            GlobalValue.mainActivityUpdate.finish();
        }
    }

    public static void closeUpdateActivity(int time) {
        Timer mTimer = new Timer();
        MyTimerTaskCloseUpdateActivity mMyTimerTask = new MyTimerTaskCloseUpdateActivity();
        mTimer.schedule(mMyTimerTask, 4000, time);    //Закроем окно обновлления
    }

    static class MyTimerTaskCloseUpdateActivity extends TimerTask {
        @Override
        public void run() {
            if (GlobalValue.mainActivityUpdate != null) {
                GlobalValue.mainActivityUpdate.finish();
            }
            Log.i("tester", "run: ");
            cancel();
        }
    }

    public static void updateCardInBase(String kod, final int ammountQti) {
        final Realm realm = Realm.getDefaultInstance();
        try {
            final RealmResults<DBCards> result = realm.where(DBCards.class)
                    .equalTo("kod", kod)
                    .findAll();

            if (result.size() > 0) {
                //Если запись в БД о билете есть, то обновим её серверными данными
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        result.get(0).setTripQty(result.get(0).getTripQty() + ammountQti);
                        realm.insertOrUpdate(result.get(0));
                        //Log.i("tester", "Update: " + cards.getCardsData().getData().get(position).getKod() + "  " + cards.getCardsData().getData().get(position).getName() + " " + cards.getCardsData().getData().get(position).getDateStart());
                    }
                });
            }
        } finally {
            realm.close();
        }
    }

    /**
     * Обновляет в базе все
     */
    public static void updateAll() {
        GlobalValue.processUpdate = true;
        GlobalValue.mainActivity.openUpdateActivity();

        MyTask mt;
        mt = new MyTask();
        mt.execute();
    }

    /**
     * Поток для обновления, но все равно UI виснет зараза при получении больших данных по билетам...
     * проблема при выполнении записи и поиска в Realm
     */
    public static class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            updateDriver();

            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            updateCard();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }
}
