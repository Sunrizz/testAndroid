package ru.ithaus.ticket.other;

import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelDB.DBCache;

import io.realm.Realm;

/**
 * Предназначен для кэширования всех данных, которые отправляются на сервер
 * Created by us on 30.07.2017.
 */

public class MyCache {

    public static void addLogIn(final String login, final String password){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBCache dbCache = realm.createObject(DBCache.class);
                dbCache.setNameCommand("auth");
                dbCache.setLogin(login);
                dbCache.setPassword(password);
                dbCache.setUserId(GlobalValue.userId);
                realm.insertOrUpdate(dbCache);
            }
        });
    }

    public static void addTrackStart(final String dateStart, final String lat, final String lon, final int userId){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBCache dbCache = realm.createObject(DBCache.class);
                dbCache.setNameCommand("trackStart");
                dbCache.setDate(dateStart);
                dbCache.setLat(lat);
                dbCache.setLon(lon);
                dbCache.setLogin(GlobalValue.login);
                dbCache.setPassword(GlobalValue.password);
                dbCache.setUserId(userId);
                realm.insertOrUpdate(dbCache);
            }
        });
    }

    public static void addTrackData(final int mileage, final String date, final String lat, final String lon, final int userId){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBCache dbCache = realm.createObject(DBCache.class);
                dbCache.setNameCommand("trackData");
                dbCache.setMileage(mileage);
                dbCache.setDate(date);
                dbCache.setLat(lat);
                dbCache.setLon(lon);
                dbCache.setLogin(GlobalValue.login);
                dbCache.setPassword(GlobalValue.password);
                dbCache.setUserId(userId);
                realm.insertOrUpdate(dbCache);
            }
        });
    }

    public static void addTrackEnd(final String dateEnd, final String lat, final String lon, final int userId){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBCache dbCache = realm.createObject(DBCache.class);
                dbCache.setNameCommand("trackEnd");
                dbCache.setDate(dateEnd);
                dbCache.setLat(lat);
                dbCache.setLon(lon);
                dbCache.setLogin(GlobalValue.login);
                dbCache.setPassword(GlobalValue.password);
                dbCache.setUserId(userId);
                realm.insertOrUpdate(dbCache);
            }
        });
    }

    public static void addTripStart(final String kod, final String dateStart, final String lat, final String lon, final String passengerQty, final int userId){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBCache dbCache = realm.createObject(DBCache.class);
                dbCache.setNameCommand("tripStart");
                dbCache.setDate(dateStart);
                dbCache.setLat(lat);
                dbCache.setLon(lon);
                dbCache.setPassengerQty(passengerQty);
                dbCache.setLogin(GlobalValue.login);
                dbCache.setPassword(GlobalValue.password);
                dbCache.setUserId(userId);
                dbCache.setKod(kod);
                realm.insertOrUpdate(dbCache);
            }
        });
    }
}
