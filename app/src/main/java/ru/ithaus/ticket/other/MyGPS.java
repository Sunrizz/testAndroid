package ru.ithaus.ticket.other;

import android.util.Log;
import android.widget.Toast;

import ru.ithaus.ticket.app.GlobalValue;
import ru.ithaus.ticket.modelApi.TrackStart;
import ru.ithaus.ticket.modelDB.DBCache;
import ru.ithaus.ticket.app.MyApp;
import ru.ithaus.ticket.modelApi.TrackData;
import ru.ithaus.ticket.modelApi.TrackEnd;
import ru.ithaus.ticket.modelApi.TripStart;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by us on 26.07.2017.
 */

public class MyGPS {

    /**
     * Отправка API TrackStart
     * параметры совпадают с API
     *
     * @param dateStart
     * @param lat
     * @param lon
     * @param userId           идентификатор юзера который является источником действия, при сливе кэша может отличаться от id полученного при авторизации
     * @param acceptCache=true записать данные в кэш в случае неотправки
     */
    public static void trackSatrtOnServer(final String dateStart, final String lat, final String lon, final int userId, final boolean acceptCache, final String dbDate) {
        //Отправка запроса TrackStart
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("dateStart", dateStart);
        parameters.put("lat", lat);
        parameters.put("lon", lon);
        parameters.put("user_id", "" + userId);

        MyApp.getApiTrackStartPost().getData(parameters, GlobalValue.bearerForAuth + GlobalValue.token).enqueue(new Callback<TrackStart>() {
            @Override
            public void onResponse(Call<TrackStart> call, Response<TrackStart> response) {

                if (response.body() != null) {
                    if (dbDate != null) {
                        updateCache(dbDate, "tripStart");
                    }
                }
            }

            @Override
            public void onFailure(Call<TrackStart> call, Throwable t) {
                if (acceptCache) {
                    MyCache.addTrackStart(dateStart, lat, lon, userId);
                }
                Toast.makeText(GlobalValue.mainActivity, "ОШИБКА СЕТИ!!!", Toast.LENGTH_SHORT).show();
                Log.i("tester", "error:   " + t.toString());
            }
        });
    }

    /**
     * Отправка API TrackStart
     * параметры совпадают с API
     *
     * @param dateStart
     * @param lat
     * @param lon
     * @param mileage
     * @param userId           идентификатор юзера который является источником действия, при сливе кэша может отличаться от id полученного при авторизации
     * @param acceptCache=true записать данные в кэш в случае неотправки
     */
    public static void trackDataOnServer(final String dateStart, final String lat, final String lon, final int mileage, final int userId, final boolean acceptCache, final String dbDate) {

        Map<String, String> parameters = new HashMap<String, String>();
        //parameters.put("mileage", "" + mileage);
        parameters.put("data", "[{\"lat\":" + lat + ",\"lon\":" + lon + ",\"date\":\"" + dateStart + "\",\"mileage\":" + mileage + "}]");
        parameters.put("user_id", "" + userId);

        Log.i("tester", "trackDataOnServer: "+parameters.get("data").toString());

        MyApp.getApiTrackDataPost().getData(parameters, GlobalValue.bearerForAuth + GlobalValue.token).enqueue(new Callback<TrackData>() {
            @Override
            public void onResponse(Call<TrackData> call, Response<TrackData> response) {

                if (response.body() != null) {
                    if (dbDate != null) {
                        updateCache(dbDate, "trackData");
                    }
                }
                Log.i("tester", "onResponse: данные ушли trackDataOnServer");
            }

            @Override
            public void onFailure(Call<TrackData> call, Throwable t) {
                if (acceptCache) {
                    MyCache.addTrackData(mileage, dateStart, lat, lon, userId);
                }
                Toast.makeText(GlobalValue.mainActivity, "ОШИБКА СЕТИ!!! trackDataOnServer", Toast.LENGTH_SHORT).show();
                Log.i("tester", "error:   " + t.toString());
            }
        });
    }

    /**
     * Отправка API TrackStart
     * параметры совпадают с API
     *
     * @param dateEnd
     * @param lat
     * @param lon
     * @param userId           идентификатор юзера который является источником действия, при сливе кэша может отличаться от id полученного при авторизации
     * @param acceptCache=true записать данные в кэш в случае неотправки
     */
    public static void trackEndOnServer(final String dateEnd, final String lat, final String lon, final int userId, final boolean acceptCache, final String dbDate) {

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("dateEnd", dateEnd);
        parameters.put("lat", lat);
        parameters.put("lon", lon);
        parameters.put("user_id", "" + userId);


        MyApp.getApiTrackEndPost().getData(parameters, GlobalValue.bearerForAuth + GlobalValue.token).enqueue(new Callback<TrackEnd>() {
            @Override
            public void onResponse(Call<TrackEnd> call, Response<TrackEnd> response) {
                if (response.body() != null) {
                    if (dbDate != null) {
                        updateCache(dbDate, "trackEnd");
                    }
                }
            }

            @Override
            public void onFailure(Call<TrackEnd> call, Throwable t) {
                if (acceptCache) {
                    MyCache.addTrackEnd(dateEnd, lat, lon, userId);
                }
                //Toast.makeText(GlobalValue.mainActivity, "ОШИБКА СЕТИ!!!", Toast.LENGTH_SHORT).show();
                Log.i("tester", "error:   " + t.toString());
            }
        });
    }

    /**
     * Отправка API TripStart
     * параметры совпадают с API
     *
     * @param kod
     * @param dateStart
     * @param lat
     * @param lon
     * @param passengerQty
     * @param userId           идентификатор юзера который является источником действия, при сливе кэша может отличаться от id полученного при авторизации
     * @param acceptCache=true записать данные в кэш в случае неотправки
     */
    public static void tripStartOnServer(final String kod, final String dateStart, final String lat, final String lon, final String passengerQty, final int userId, final boolean acceptCache, final String dbDate) {

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("dateStart", dateStart);
        parameters.put("lat", lat);
        parameters.put("lon", lon);
        parameters.put("kod", kod);
        parameters.put("passengerQty", passengerQty);
        parameters.put("user_id", "" + userId);

        Log.i("tester", "tripSatrtOnServer: passengerQty = " + passengerQty);

        MyApp.getApiTripStartPost().getData(parameters, GlobalValue.bearerForAuth + GlobalValue.token).enqueue(new Callback<TripStart>() {
            @Override
            public void onResponse(Call<TripStart> call, Response<TripStart> response) {

                final TripStart tripStart = response.body();  //Получение ответа с номером трека

                if (tripStart == null) {
                    Toast.makeText(GlobalValue.mainActivity, "Ошибка списания с карты", Toast.LENGTH_SHORT).show();
                }
                if (response.body() != null) {
                    if (dbDate != null) {
                        updateCache(dbDate, "tripStart");
                    }
                }

            }

            @Override
            public void onFailure(Call<TripStart> call, Throwable t) {
                Toast.makeText(GlobalValue.mainActivity, "ОШИБКА СЕТИ!!!", Toast.LENGTH_SHORT).show();
                Log.i("tester", "error:   " + t.toString());
                if (acceptCache) {
                    MyCache.addTripStart(kod, dateStart, lat, lon, passengerQty, userId);
                }
            }
        });
    }


    /**
     * Вычисляет расстояние в метрах от точки lat1/lon1 до lat2/lon2
     *
     * @param lat1 координаты GPS
     * @param lon1
     * @param lat2
     * @param lon2
     * @return Расстояние в метрах от точки lat1/lon1 до lat2/lon2
     */
    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double p = 0.017453292519943295;    // Math.PI / 180
        double a = 0.5 - Math.cos((lat2 - lat1) * p) / 2 +
                Math.cos(lat1 * p) * Math.cos(lat2 * p) *
                        (1 - Math.cos((lon2 - lon1) * p)) / 2;

        return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
    }

    /**
     * Сделать пометку что запись кэша отправлена
     * по следующим значениям находим запись в базе
     *
     * @param date        поиск по дате
     * @param nameCommand поиск по команде
     */
    private static void updateCache(final String date, final String nameCommand) {

        GlobalValue.mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Realm realm = Realm.getDefaultInstance();
                try {
                    final RealmResults<DBCache> result = realm.where(DBCache.class)
                            .equalTo("date", date)
                            .equalTo("nameCommand", nameCommand)
                            .equalTo("sync", 0)
                            .findAll();

                    if (result.size() == 0) {
                        return;
                    }

                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            try {
                                result.get(0).setSync(1);
                                realm.insertOrUpdate(result.get(0));
                            } catch (Exception e) {

                            }
                        }
                    });
                }finally {
                    realm.close();
                }
            }
        });
    }


}
