package ru.ithaus.ticket.animate;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

/**
 * Created by us on 15.02.2017.
 */

public class AnimateButtonUpDown {

    protected Animation anim;
    protected ScaleAnimation shrink, grow, up, down;

    public AnimateButtonUpDown(final View button) {


        float sizeAnimation = 1.2f;
        up = new ScaleAnimation(1.0f, sizeAnimation, 1.0f, sizeAnimation,
                Animation.RELATIVE_TO_SELF, 0.8f, Animation.RELATIVE_TO_SELF, 0.8f);
        up.setDuration(200);
        up.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                button.startAnimation(down);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        down = new ScaleAnimation(sizeAnimation, 1.0f, sizeAnimation, 1.0f,
                Animation.RELATIVE_TO_SELF, 0.8f, Animation.RELATIVE_TO_SELF, 0.8f);
        down.setDuration(200);

        button.startAnimation(up);
    }
}
