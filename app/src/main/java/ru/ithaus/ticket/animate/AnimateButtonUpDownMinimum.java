package ru.ithaus.ticket.animate;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

/**
 * Created by us on 15.02.2017.
 */

public class AnimateButtonUpDownMinimum {

    protected Animation anim;
    protected ScaleAnimation shrink, grow, up, down;

    public AnimateButtonUpDownMinimum(final View button) {


        float sizeAnimation = 1f;
        up = new ScaleAnimation(sizeAnimation, sizeAnimation, 0, sizeAnimation,
                Animation.RELATIVE_TO_SELF, sizeAnimation, Animation.RELATIVE_TO_SELF, 0);
        up.setDuration(200);
        up.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //button.startAnimation(down);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        down = new ScaleAnimation(sizeAnimation, 1.0f, sizeAnimation, 1.0f,
                Animation.RELATIVE_TO_SELF, sizeAnimation, Animation.RELATIVE_TO_SELF, sizeAnimation);
        down.setDuration(500);

        button.startAnimation(up);
    }
}
