package ru.ithaus.ticket.app;

import android.app.Application;

import ru.ithaus.ticket.api.ApiCardsGet;
import ru.ithaus.ticket.api.ApiTrackDataPost;
import ru.ithaus.ticket.api.ApiTrackEndPost;
import ru.ithaus.ticket.api.ApiTripStartPost;
import ru.ithaus.ticket.api.ApiAuthPost;
import ru.ithaus.ticket.api.ApiCardsDataGet;
import ru.ithaus.ticket.api.ApiCardsDataLastDateGet;
import ru.ithaus.ticket.api.ApiDriverGet;
import ru.ithaus.ticket.api.ApiTrackStartPost;

import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by us on 20.07.2017.
 */

public class MyApp extends Application {

    private static ApiAuthPost   apiAuth;
    private static ApiDriverGet  apiDriverGet;
    private static ApiCardsDataGet apiCardsDataGet;
    private static ApiCardsDataLastDateGet apiCardsDataLastDateGet;

    private static ApiTrackDataPost apiTrackDataPost;
    private static ApiTrackEndPost apiTrackEndPost;
    private static ApiTrackStartPost apiTrackStartPost;
    private static ApiTripStartPost apiTripStartPost;
    private static ApiCardsGet apiCardsGet;

    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();


        Realm.init(this);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://qr.ithaus.ru/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiAuth = retrofit.create(ApiAuthPost.class);
        apiDriverGet = retrofit.create(ApiDriverGet.class);
        apiCardsDataGet = retrofit.create(ApiCardsDataGet.class);

        apiTrackDataPost = retrofit.create(ApiTrackDataPost.class);
        apiTrackEndPost = retrofit.create(ApiTrackEndPost.class);
        apiTrackStartPost = retrofit.create(ApiTrackStartPost.class);
        apiTripStartPost = retrofit.create(ApiTripStartPost.class);
        apiCardsGet = retrofit.create(ApiCardsGet.class);

        apiCardsDataLastDateGet = retrofit.create(ApiCardsDataLastDateGet.class);

    }

    public static ApiAuthPost getApiAuthPost() {
        return apiAuth;
    }

    public static ApiDriverGet getApiDriverGet() {
        return apiDriverGet;
    }

    public static ApiCardsDataGet getApiCardsDataGet() {
        return apiCardsDataGet;
    }

    public static ApiTrackDataPost getApiTrackDataPost() {
        return apiTrackDataPost;
    }

    public static ApiTrackEndPost getApiTrackEndPost() {
        return apiTrackEndPost;
    }

    public static ApiTrackStartPost getApiTrackStartPost() {
        return apiTrackStartPost;
    }

    public static ApiTripStartPost getApiTripStartPost() {
        return apiTripStartPost;
    }

    public static ApiCardsGet getApiCardsGet() {
        return apiCardsGet;
    }

    public static ApiCardsDataLastDateGet getApiCardsDataLastDateGet() {
        return apiCardsDataLastDateGet;
    }
}
