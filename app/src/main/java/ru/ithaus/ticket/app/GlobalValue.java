package ru.ithaus.ticket.app;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import ru.ithaus.ticket.ui.MainActivity;
import ru.ithaus.ticket.ui.MainActivityUpdate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by us on 19.07.2017.
 * Хранит основные переменные, которые доступны в любом месте приложения
 */

public class GlobalValue {
    public static int mileage = 0; //пройденный путь нарастающим итогом
    public static MainActivity mainActivity;
    public static MainActivityUpdate mainActivityUpdate = null;
    public static String token=null;     //Токен авторизации
    public static String nameDriver;     //Имя водителя полученное при авторизации
    public static String cardNum;        //Номер карты по которой происходит поездка
    public static String cardDateEnd;    //Номер карты по которой происходит поездка
    public static String cardStatus;     //Статус карты по которой происходит поездка
    public static int    cardType;       //Тип карты по которой происходит поездка
    public static int    ammountWay;     //Количество оставшихся поездок карты по которой происходит поездка
    public static int    ammount;        //Списано с последней поезди
    public static int    trackId=-1;     //Списано с последней поезди

    public static int    userId=-1;      //id текущего авторизованного пользователя

    public static boolean processUpdate = false; //Запущено ли сейчас обновление

    public static String login;          //текущий логин
    public static String password;       //текущий пароль

    public static boolean offlineMode=true;  //Индикатор режима работы - true без интернета, false с интернетом

    public static boolean checkCardMode = false;

    public static double lat=0, lon=0; //Текущие координаты GPS

    public static String bearerForAuth = "Bearer ";  //Эта приставка используется при передаче токена авторизации

    public static final String APP_PREFERENCES   = "mysettings";  //Константы для обращения к SharedPreferences
    public static final String APP_PREFERENCES_LAST_DATE_CARD  = "LAST_DATE_CARD"; //Константы для обращения к SharedPreferences
    public static String lastDate = "";  //Дата последнего обновления при старте восстановим из SharedPreferences

    /**
     * Открывает запрос на Permission
     */
    public static void requestRead_Internet_StatePermission(String permission) {

        int rc = ActivityCompat.checkSelfPermission(mainActivity, permission);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            return;
        }

        final String[] permissions = new String[]{permission};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(mainActivity,
                permission)) {
            ActivityCompat.requestPermissions(mainActivity, permissions, 1);
            return;
        }

        final Activity thisActivity = mainActivity;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        1);
            }
        };

    }

    /**
     * Возвращает количество поездок прописью
     * Например Нет поездок, 1 поездка, 2 поездки и т.д.
     */
    public static String getTextFromAmmountWay(int ammountWay) {

        String result = "";

        String strOldNumeric = "" + ammountWay;  //Храним последний символ из числа

        String strPreOldNumeric = ""; //Храним пред последний символ из числа
        if(ammountWay>9) {
            strPreOldNumeric = strOldNumeric.substring(strOldNumeric.length() - 2, strOldNumeric.length() - 1);
        }

        strOldNumeric = strOldNumeric.substring(strOldNumeric.length() - 1);

        if (ammountWay == 0) {
            return "Нет поездок";
        }

        if (ammountWay == -99) {
            return "до даты карты";
        }

        if (strPreOldNumeric.equals("1")) {
            result = " поездок";
        } else {
            if (strOldNumeric.equals("1")) {
                result = " поездка";
            }
            if (strOldNumeric.equals("2")) {
                result = " поездки";
            }
            if (strOldNumeric.equals("3")) {
                result = " поездки";
            }
            if (strOldNumeric.equals("4")) {
                result = " поездки";
            }
            if (strOldNumeric.equals("5")) {
                result = " поездок";
            }
            if (strOldNumeric.equals("6")) {
                result = " поездок";
            }
            if (strOldNumeric.equals("7")) {
                result = " поездок";
            }
            if (strOldNumeric.equals("8")) {
                result = " поездок";
            }
            if (strOldNumeric.equals("9")) {
                result = " поездок";
            }
            if (strOldNumeric.equals("0")) {
                result = " поездок";
            }
        }

        return "" + ammountWay + result;
    }

    /**
     * Получение форматированной строки даты
     * @param year  год
     * @param month месяц
     * @param day   день
     * @param hh    часы
     * @param mm    минуты
     * @return   возвращает строку вида «ггггммддччммсс»
     */
    public static String getDateFormatUser(int year, int month, int day, int hh, int mm){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hh, mm);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return simpleDateFormat.format(calendar.getTime());
    }

    /**
     * Получение форматированной строки c текущей датой
     * @return   возвращает строку вида «ггггммддччммсс»
     */
    public static String getCurrentDateFormatUser(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        return simpleDateFormat.format(calendar.getTime());
    }

    /**
     * Округление до х зна кпосле запятой
     * @param latLon
     * @return
     */
    public static String getRoundGPS(double latLon){
        return String.format("%(.4f", latLon);
    }

    public static Date parseDateFromString(String myDate){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        Date date=null;
        try {
            date = format.parse(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

}
